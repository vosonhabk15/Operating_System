#include "procmem.h"
#include <linux/kernel.h>
#include <sys/syscall.h>
#define SIZE 100

long procmem (pid_t pid, struct proc_segs * info) {
	unsigned long buff[SIZE];
	long sysvalue;
	sysvalue = syscall(546,pid,buff);
	info->mssv = buff[0];
	info->start_code = buff[1];
	info->end_code = buff[2];
	info->start_data = buff[3];
	info->end_data = buff[4];
	info->start_heap = buff[5];
	info->end_heap = buff[6];
	info->start_stack = buff[7];
	return 0;
}

